%title: PDFTK (PDF Toolkit)
%author: dp403aan

# PDFTK - A handy tool for manipulating PDF


<br>

## Presentation

According to its man page, if PDF is electronic paper, 
then PDFtk is an electronic staple-remover, hole-punch, binder, 
secret-decoder-ring, and X-Ray-glasses.

<br>

## Installation

`sudo apt update`
`sudo apt install pdftk`

------------------------------------------

# PDFTK - A handy tool for manipulating PDF

<br>

* Extraire la page 1 de file.pdf   

`pdftk file.pdf cat 1 output 1.pdf`

<br>

* Séparer toutes les pages d'un pdf

`pdftk file.pdf burst`

<br>

* Assembler plusieurs pdf en un seul

`pdftk \*.pdf output compilation.pdf`

<br>

* Extraire les pages paires d'un pdf

`pdftk file.pdf cat even output even.pdf`

------------------------------------------

# PDFTK - A handy tool for manipulating PDF

<br>

* Extraire les pages impaires d'un pdf

`pdftk file.pdf cat odd output odd.pdf`
<br>

* Assembler les pages 1 à 20 de file1.pdf et les pages 10 à 35 de file2.pdf

`pdftk A=file1.pdf B=file2.pdf cat A1-20 B10-35 output mixed-file.pdf`

<br>

* Inverser les pages d'un pdf

`pdftk file.pdf cat End-1 output reverse.pdf`

<br>

* Pivoter de 90° à droite la page 1 de pdf

`pdftk file.pdf cat 1east output rotate.pdf`

<br>

* Extraire les pages paires et tout en les pivotant de 90° sur la gauche

`pdftk A=file.pdf cat Aevenleft output evenleft.pdf`

<br>

* Supprimer le mot de passe d'un pdf

`pdftk secured.pdf input_pw my_pass output unsecured.pdf`

------------------------------------------

# PDFTK - A handy tool for manipulating PDF

<br>

## Compléments

* `man pdftk`

* [le site de pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)

* [sur le site d'ubuntu](https://doc.ubuntu-fr.org/pdftk)

------------------------------------------

# PDFTK - A handy tool for manipulating PDF

<br>

MERCI !!

